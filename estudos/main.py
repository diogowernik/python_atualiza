
# importar pacotes
import yfinance as yf
import pandas as pd
import pandas_datareader.data as pdr
import datetime as dt

yf.pdr_override() # configuração do Yahoo Finance
pd.set_option('precision', 7)  # tamanho da casas decimasi
pd.set_option('use_inf_as_na', True)  # converter os valores 'inf' em NaN


data_atual = dt.datetime.now() # Pegar data atual
dt_start = data_atual + dt.timedelta(days=-5)  # inicio da data da cotação de 5 dias atras, para poder calcular a varia do dia
dt_end = data_atual # fim da data de cotação, sendo a data atual

codigos = ['SAPR4.SA', 'ITSA4.SA', 'CCRO3.SA'] # definir a lista de codigos para buscar a cotação,,, eu costumo nao deixar essa lista passar de 200 ou 300, para nao dar erro ao pegar a cotação de muito ativos

data = pdr.get_data_yahoo(codigos, start=dt_start, end=dt_end, )['Adj Close'] # buscar as cotações em lote de codigos, e somente exibir o preço ajustao ('Adj Close')
 
 for codigo in codigos: # loop por codigo para calcular a valorização do dia

	    df = pd.DataFrame(data=data[codigo]) # transformar em DataFrame para melhor ajuste dos dados
	    df['VARIACAO'] = df.pct_change() * 100 # calcular o percentual em relação ao valor do dia anterior
	    df.dropna(inplace=True)  # remover duplicades por indices - neste caso o Indice é a Data
	    df.fillna(0, inplace=True)  # limpar colunas NaN # Converter os valores NaN em 0.00
	    df.rename(columns={codigo: 'FECHAMENTO'}, inplace=True) # alterar a coluna 'Adj Close', para o nome o codidigo do ativo
	    df['ANTERIOR'] = df['FECHAMENTO'].shift() # coloca como coluna da mesma linha, o valor de fechamento da linha anterior, somente para facilitar o get do valor do dia anterior

	    atv_date = ''
	    atv_vlr_close = 0.00
	    atv_vlr_close_yst = 0.00
	    atv_vlr_perct = 0.00

	    for row in df.itertuples(): # ajuste para calcular corretamente o valor do dia anterior e seu percentual, pois o Yahoo Finance, tem um problema de listar o dividendo quando vai buscar a cotação, isso faz calcular errado a cotação do dia anterior
		      local_date = str(row[0])[0:10].replace('-', '')
		      if (atv_date != local_date):  # somente pegar os dados do ultimo dia, mas se repetir nao deve pegar novamente..
			          atv_date = local_date
			          atv_vlr_close = float("%.2f" % row[1])
			          atv_vlr_perct = float("%.2f" % row[2])
			          atv_vlr_close_yst = float("%.2f" % row[3])

	    print(f'Codigo: {codigo} - Data: {atv_date}- Cotação: { atv_vlr_close} - Cotação Anterior: {atv_vlr_close_yst} - Variação do Dia: {atv_vlr_perct}')
