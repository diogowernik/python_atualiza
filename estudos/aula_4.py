import yfinance as yf
import numpy as np
import pandas as pd

tickers = "ABEV3.SA ITSA4.SA WEGE3.SA USIM5.SA VALE3.SA"

carteira = yf.download(tickers, period="5y")["Adj Close"]

ibov = yf.download("^BVSP", period="5y")["Adj Close"]

carteira.dropna(inplace=True)

print(carteira)