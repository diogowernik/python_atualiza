import numpy as np
import pandas as pd
import pandas_datareader.data as web
import yfinance as yf
yf.pdr_override()

ibov = web.get_data_yahoo('^BVSP', start='2010-05-03', end='2013-03-01')

ibov.head()
ibov.tail()

print(ibov.head())
print(ibov.tail())