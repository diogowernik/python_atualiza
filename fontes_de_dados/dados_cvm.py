import pandas as pd
pd.set_option("display.max_colwidth", 150)
#pd.options.display.float_format = '{:.2f}'.format

def busca_informes_cvm(ano, mes):
  url = 'http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/inf_diario_fi_{:02d}{:02d}.csv'.format(ano,mes)
  return pd.read_csv(url, sep=';')
  
 
def busca_cadastro_cvm(ano, mes, dia):
  url = 'http://dados.cvm.gov.br/dados/FI/CAD/DADOS/inf_cadastral_fi_{}{:02d}{:02d}.csv'.format(ano, mes, dia)
  return pd.read_csv(url, sep=';', encoding='ISO-8859-1')
  
#informes_diarios = busca_informes_cvm(2020,4)

#print(informes_diarios)

cadastro_cvm = busca_cadastro_cvm(2020,5,1)

print(cadastro_cvm)
